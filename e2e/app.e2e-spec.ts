import { NgFriendsVacayPage } from './app.po';

describe('ng-friends-vacay App', () => {
  let page: NgFriendsVacayPage;

  beforeEach(() => {
    page = new NgFriendsVacayPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
