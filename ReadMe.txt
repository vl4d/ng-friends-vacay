Author: Vladimir Murray
Year: 2017
---------------------------------------------

This is the ReadMe.txt file for ng-friends-vacay.
- An Angular v4 application which was developed for the MSc Individual Project at King's College London.
- This is the software package for the Travel Friends web application. 
- It utilizes a modified ACO algorithm to produce tours for friend's travelling Europe together and will output those tours on a map of Europe with each Traveller's itinerary. 
----------------------------------------------
Folder Structure and File Description
----------------------------------------------
|-- .angular-cli.json           // configuration for angular-cli
|-- .editorconfig               // editor settings for IDE
|-- .gitignore                  // files to be ignored by source control (git)
|-- README.md                   // description on how to run the project with angular-cli
|-- karma.conf.js               // configuration for testing
|-- package-lock.json           // locked dependency information
|-- package.json                // list of all dependencies required by the project from npm (node package manager)
|-- protractor.conf.js          // configuration for testing
|-- tsconfig.json               // configurations for typescript
|-- tslint.json                 // configurations for typescript linter
|-- e2e                         // FOLDER for end to end testing
|   |-- app.e2e-spec.ts
|   |-- app.po.ts
|   |-- tsconfig.e2e.json
|-- dist                        // FOLDER containing distributed files. Software package can be served from this folder. 
|-- src                         // FOLDER containing main files of software package.
    |-- _variables.scss         // Custom scss for styling of project
    |-- favicon.ico 
    |-- index.html              // Entry point of package, the main HTML page
    |-- main.ts                 // Main Typescript module which bootstraps software package.
    |-- polyfills.ts            
    |-- styles.scss             // Styles required for project
    |-- test.ts                 // Entry point for tests
    |-- tsconfig.app.json       // application configuration
    |-- tsconfig.spec.json
    |-- typings.d.ts
    |-- app                     // FOLDER containing app modules and services
    |   |-- app-routing.module.ts       // Typescript module for routing of pages based on URL
    |   |-- app.component.html          // The html markup for the main app component
    |   |-- app.component.scss          // The styling for the main app component
    |   |-- app.component.spec.ts
    |   |-- app.component.ts            // The main app component
    |   |-- app.module.ts               // The main app module
    |   |-- configure                   // FOLDER containing the custom components for the configuration page (configures the ACO input)
    |   |   |-- config-wizard.component.html
    |   |   |-- config-wizard.component.ts
    |   |   |-- configure.component.html
    |   |   |-- configure.component.spec.ts
    |   |   |-- configure.component.ts
    |   |   |-- configure.service.spec.ts
    |   |   |-- configure.service.ts
    |   |   |-- aco-results                         // FOLDER containing the results component (Handles displaying results on the map plus itinerary)
    |   |   |   |-- aco-results.component.html
    |   |   |   |-- aco-results.component.scss
    |   |   |   |-- aco-results.component.spec.ts
    |   |   |   |-- aco-results.component.ts
    |   |   |-- user-list                           // FOLDER containing the user list component (Handles adding friends to the tour)
    |   |   |   |-- user-list.component.html
    |   |   |   |-- user-list.component.scss
    |   |   |   |-- user-list.component.spec.ts
    |   |   |   |-- user-list.component.ts
    |   |   |-- user-prefs                          // FOLDER containing the user preferences component (Handles the management of user preferences)
    |   |   |   |-- country-selector.component.ts
    |   |   |   |-- user-prefs.component.html
    |   |   |   |-- user-prefs.component.scss
    |   |   |   |-- user-prefs.component.spec.ts
    |   |   |   |-- user-prefs.component.ts
    |   |   |-- wizard                              // FOLDER containing the basic step wizard used. 
    |   |       |-- index.ts
    |   |       |-- wizard-step.component.ts
    |   |       |-- wizard.component.html
    |   |       |-- wizard.component.scss
    |   |       |-- wizard.component.ts
    |   |-- footer                                  // FOLDER containing the footer component (seen at bottom of pages)
    |   |   |-- footer.component.html
    |   |   |-- footer.component.scss
    |   |   |-- footer.component.spec.ts
    |   |   |-- footer.component.ts
    |   |-- home                                    // FOLDER containing the home page component
    |   |   |-- home.component.html
    |   |   |-- home.component.scss
    |   |   |-- home.component.spec.ts
    |   |   |-- home.component.ts
    |   |-- nav                                     // FOLDER containing the navigation component (seen at top of pages)
    |   |   |-- nav.component.html
    |   |   |-- nav.component.scss
    |   |   |-- nav.component.spec.ts
    |   |   |-- nav.component.ts
    |   |-- scripts                                 // FOLDER containing the Modified ACO Script described in paper. 
    |   |   |-- modified-aco.ts
    |   |-- utils
    |       |-- pipes
    |           |-- values.pipe.ts
    |-- assets
    |   |-- .gitkeep
    |-- data                                        // FOLDER containing the town data used within project
    |   |-- townActivities.json
    |   |-- townInfo.json
    |   |-- towns.json
    |   |-- 24cities
    |   |   |-- towns24.json
    |   |   |-- townsInfo24.json
    |   |-- 69cities
    |       |-- european69.json
    |       |-- towns69.json
    |       |-- townsInfo69.json
    |-- environments
        |-- environment.prod.ts
        |-- environment.ts

----------------------------------------------
How to Run: Option 1
---------------------------------------------

To run this software package, serve the contents of the dist folder from a web-server. 
The dist folder contains the minimised/compressed version of the code. 

If you have python installed you could navigate to the dist folder in your terminal and run:

$ python -m SimpleHTTPServer 4200


This will launch a http web server, serving the folder to port 4200.
You could then access the software package at: http://localhost:4200
---------------------------------------------
How to Run: Option 2
---------------------------------------------

To run it using angular-cli and nodejs you would need to install

Node v6 or above accessible here: https://nodejs.org/en/download/

Once node is installed it comes with its own package manager npm. 
Install angular-cli (description here: https://cli.angular.io/)  using the command: 

$ npm install angular-cli

After angular-cli is installed, the project dependencies need to be installed. Run this command from the root folder of project:

$ npm install

This installs all dependencies described in package.json.
Final step is to serve the contents of the project. Angular CLI comes with a web server which allows you to serve the contents using:

$ ng serve -o

This normally serves the project on port 4200. So it should be accessible here: http://localhost:4200 