import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import _forEach from 'lodash/forEach';
import _merge from 'lodash/merge';
import _clone from 'lodash/clone';
import _includes from 'lodash/includes';
import _intersection from 'lodash/intersection';
import _intersectionWith from 'lodash/intersectionWith';
import _filter from 'lodash/filter';
import _uniqWith from 'lodash/uniqWith';

/**
 * The ant object that will be participating in the ACO
 *  - keeps track of the ant specific variables
 */
class Ant {
  public tour: Array<string> = [];
  public travellerIndex: number;
  public visited: any = {};
  constructor(travellerIndex: number) {
    this.travellerIndex = travellerIndex;
  }

  // Add town to the tour
  public visitTown(key: string) {
    if (key) {
      this.tour.push(key);
      this.visited[key] = true;
    }

  }

  /**
   * getTourLength - Returns current length of tour.
   *
   * @param {*} graph - object with keys of cities and cost values
   * @returns
   * @memberof Ant
   */
  public getTourLength(graph: any) {
    // Modify as this pertains to distance between towns
    let length = 0;
    if (this.tour.length) {
      // length of Last town to start town
      length = graph[this.tour[this.tour.length - 1]][this.tour[0]];
      // length of All towns inbetween.
      for (let i = 0; i < this.tour.length - 1; i++) {
        length += graph[this.tour[i]][this.tour[i + 1]];
      }
    }

    return length;
  }

  /**
   * clear() Method to remove all the visited cities.
   *
   */
  public clear() {
    this.tour = [];
    this.visited = {};
  }
}

/**
 *  Contraint on Budget - All the budget is just a value based on the distance at this point?
 *  Preferences - Each place has a list of activities, which would be more attractive
 *  Country - Each city within a country would be more appealing as well.
 *
 *
 * @class ModifiedAco
 */

class ModifiedAco {

  // Algorithm parameters:
  // original amount of trail
  private c = 1.0;

  // trail preference
  private alpha = 2;

  // greedy preference
  private beta = 3;

  // meeting preference - recommend 1 or less.
  private gamma = 2;

  // trail evaporation coefficient
  private evaporation = 0.5;

  // weight of the preferences (control to not make distance based on preference too large)
  private prefWeight = 0.4;

  // new trail deposit coefficient;
  private Q = 500;

  // probability of pure random selection of the next town
  private pr = 0.01;

  // Reasonable number of iterations
  // - results typically settle down by 500
  private maxIterations = 1000;

  // Reasonable time interval for meeting (in minutes)
  private meetingTimeInterval = 30;

  // Base graph, object of objects
  private graph: any = {};

  // Set min and max distances in the graph
  private graphMin = 200.0; // 200 new graph
  private graphMax = 800.0; // 700 new graph
  private graphs: any = []; // graphs per traveller
  private townInfo: any = {};
  private travellers: Array<any> = [];
  private rand: number = Math.random();

  // Traveller variables that change over time.
  private N = []; // array subgraph of possible moves per ant;
  private trails: any = []; // pheromone trails on graph per ant
  private probs: any = [];  // probability to next possible town per ant based on N
  private ants: Array<any> = [];
  public bestTours: Array<any> = [];
  public bestToursWithMeetings: Array<any> = [];
  public bestToursCostWithMeetings: any = {};
  public bestToursWithMostMeetings: Array<any> = [];
  public bestToursCostWithMostMeetings: any = {};

  constructor(graph: any, townInfo: any, travellers: Array<any>) {

    /**
     * Graph should be json object with connected towns
     *
     * Graph with wieghts trying to be minimised.
     * eg graph = {
     *      "town 1": {
     *              "town 1": 0,
     *              "town 2": 1.5
     *            },
     *      "town 2": {
     *              "town 1": 1.5,
     *              "town 2": 0
     *            }
     *    }
     *
     *  Graph with Town Activities
     *  eg townInfo = {
     *      "town 1": {
     *                  Country: "France",
     *                  time: 5000 (mins)
     *                  Activities: ["museum"],
     *                  Cost: 130,
     *                  latlng: "" // or array?
     *              },
     *      "town 2": {
     *                  Country: "UK",
     *                  Activities: ["historical buildings", "beaches"],
     *                  time: 3000 (mins)
     *                  Cost: 140,
     *                  latlng: "" // or array?
     *              }
     *    }
     *
     *  Travellers with their preferences
     *  eg travellers: [
     *    {
     *      _id: '1',
     *      name: 'Vlad-Tester',
     *      activityPrefs: [ {name: string, weight: number}],
     *      countryPrefs: [],
     *      budget: 100.00,
     *      startTown: "Madrid",
     *      time: 7 * 24 * 60
     *    },
     *    {
     *      _id: '2',
     *      name: 'User1-Tester',
     *      activityPrefs: [],
     *      countryPrefs: [],
     *      budget: 80.00,
     *      startTown: "London",
     *      time: 7 * 24 * 60
     *    }
     *   ];
     *
     */

    this.bestToursCostWithMeetings.cost =  -1;
    this.bestToursCostWithMeetings.meetings = [];
    this.bestToursCostWithMostMeetings.cost =  -1;
    this.bestToursCostWithMostMeetings.meetings = [];

    this.travellers = travellers;
    this.graph = graph || {};
    this.townInfo = townInfo || {};
    // initialize ant per traveller
    this.ants = [];
    // TODO generate graph's Min and Max at the moment it is hardcoded.
    for (let i = 0; i < this.travellers.length; i++) {
      this.ants.push(new Ant(i));
    }
  }

  private setupAnts() {
    for (let i = 0; i < this.ants.length; i++) {
      this.ants[i].clear(); // reset ants

      // set ant to on start town
      if (this.travellers[i].startTown) {
        this.ants[i].visitTown(this.travellers[i].startTown);
      } else {
        // default random town from graph.
        const towns = Object.keys(this.graph);
        const randTownIndex = getRandomIntInclusive(0, towns.length - 1)
        this.ants[i].visitTown(towns[randTownIndex]);
      }
      // update reacheable neighbouring cities based on time, cost.
      this.updateNeighbours(this.ants[i]);
    }
  }

  /**
   * possibleMovesLeft - checks if the given ant index or the overall algorithm has
   *      any possible moves left. Uses this.N
   *
   * @private
   * @param {number} [index=-1]
   * @returns
   * @memberof modifiedACO
   */
  private possibleMovesLeft(index: number = -1) {
    let result = false;

    if (index > -1) {
      if (this.N[index] && Object.keys(this.N[index]).length) return true;
      return result;
    }

    for (let i = 0; i < this.ants.length; i++) {
      if (Object.keys(this.N[i]).length) {
        result = true;
        break;
      }
    }

    return result;
  }

  private getCost(tour: Array<string>) {
    // go through tour and add the cost of visiting each town.
    // TODO improvement cost of travelling from town to town is diff based on distance?
    let cost = 0.0;
    let tourEnd = tour.length;
    // if (tour.length > 1 && tour[0] === tour[tour.length - 1]) {
    //   tourEnd -= 1;
    // }

    for (let i = 0; i < tourEnd; i++) {
      if(i === 0) continue;
      // if(i === tourEnd) continue;
      cost += this.townInfo[tour[i]].cost;
    }

    return cost;
  }

  private getTime(tour: Array<string>) {
    // Go through tour and add the time of visiting each town.
    // TODO improvement for time to multiply by time by each activity
    //    done based on traveller preference and activity in town.
    //    Also based on time to travel to the town from a specific town.
    let time = 0.0;

    for (let i = 0; i < tour.length; i++) {
      time += this.townInfo[tour[i]].time;
    }

    return time;
  }

  private updateNeighbours(ant: Ant) {
    this.N[ant.travellerIndex] = {};

    // 1. calculate cost for current trip so far.
    // 2. calculate time of current trip so far.
    // 3. remove all cities where the cost and time to get there grater than the max cost and time set in preferences

    // TODO add travel time and cost between cities.
    const costSoFar = this.getCost(ant.tour);
    const timeElapsed = this.getTime(ant.tour);
    const budgetRemaining = this.travellers[ant.travellerIndex].budget - costSoFar;
    const timeRemaining = this.travellers[ant.travellerIndex].time - timeElapsed;
    const currentTown = ant.tour[ant.tour.length - 1];

    // loop through adjacent towns and see if travelling there would exceed the budget or time constraint
    // TODO add the travel back to origin time and budget to this check (ensuring the traveller can get back home)
    _forEach(this.graphs[ant.travellerIndex][currentTown], (info, town) => {
      let townVisited = false;
      for (let i = 0; i < this.ants.length; i++) {
        if (_includes(this.ants[i].tour, town)) {
          townVisited = true;
          break;
        }
      }

      if (!townVisited) {
        if (this.townInfo[town].time < timeRemaining && this.townInfo[town].cost < budgetRemaining) {
          this.N[ant.travellerIndex][town] = true;
        }
      }
    });
  }

  /**
   * moveAnts() - continuously moves ants while there are possible neighbour node to visit
   */
  private moveAnts() {
    // each ant follows trails...
    while (this.possibleMovesLeft()) {
      for (let i = 0; i < this.ants.length; i++) {
        if (this.possibleMovesLeft(i)) {
          this.ants[i].visitTown(this.selectNextTown(this.ants[i]));
          // update reacheable neighbouring cities based on time, cost.
          this.updateNeighbours(this.ants[i]);
        }
      }
    }

    // Ensure all the ants go back to their startTown if no possible moves are available.
    for (let i = 0; i < this.ants.length; i++) {
      if (this.ants[i].tour && this.ants[i].tour.length > 1) {
        this.ants[i].visitTown(this.travellers[i].startTown || this.ants[i].tour[0]);
      }
    }

  }

  /**
   * meetingProbability - Get traverse time so far of all ants.
   *  if they are close (this.meetingTimeInterval mins overlap) when going to the nextTown increase probablility to town.
   */
  private meetingProbability(ant: Ant, currentTown: string, nextTown: string) {
    // Only if ant has visited 2 places otherwise return 1.
    if (ant.tour.length < 3) return 1;

    const timeWithinTown = this.getTime(ant.tour) + this.townInfo[nextTown].time;
    const possibleMeeters = [];
    let meetsPossibile = 0;
    for (let i = 0; i < this.ants.length; i++) {
      // continue if the current ant
      if (i === ant.travellerIndex) continue;
      // continue if ant already visited the town in question.
      if (this.ants[i].visited[nextTown]) continue;
      // continue if town not in ant's neighbours
      if (!this.N[i][nextTown]) continue;

      // continue if this ant met other ant already
      const meetings = this.getMeetings(i);
      if (meetings && meetings.length) {
        // The meetings Array contains objects of signature
        // {
        //     ants: [antIndex, j],
        //     town,
        //     time: timeAntI
        // }
        let metWithAnt = false;
        for (let meetingIndex = 0; meetingIndex < meetings.length; meetingIndex++) {
          if (meetings[meetingIndex].town === nextTown && meetings[meetingIndex].ants[1] === i) {
            metWithAnt = true;
            break;
          }
        }
        if (metWithAnt) continue;
      }

      // At this point the ant hasnt visited the town and they have it in their possible next moves.
      // Check if the times overlap and increase depending on the
      const _timeWithinTown = this.getTime(this.ants[i].tour) + this.townInfo[nextTown].time;
      // add the time to the town.
      const timeDiff = timeWithinTown - _timeWithinTown;
      if (timeDiff <= this.meetingTimeInterval && timeDiff >= -this.meetingTimeInterval) {
        // Can meet if stays within 2 hours.
        meetsPossibile++;
      }
    }

    if (!meetsPossibile) return 1; // return 1 if no meeting is possible
    return Math.pow(1 + (meetsPossibile / this.ants.length), this.gamma);
  }

  /**
   *  getMeetings - Get meetings that couldve happened between two ants
   *  if they are close (this.meetingTimeInterval mins overlap) when going to the nextTown increase probablility to town.
   */
  private getMeetings(index: number = -1) {
    const meetings = [];
    const _self = this;
    function getMeetingWithAnt(antIndex: number) {
      for (let j = 0; j < _self.ants.length; j++) {
        if (antIndex === j || _self.ants[j].tour.length < 3) continue;
        // Get the similar towns between two ants
        const commonTowns = _intersection(_self.ants[antIndex].tour, _self.ants[j].tour);

        // get the time from start to that town + time in that town for both ants.
        // if they overlap by +/- this.meetingTimeInterval mins then they met in that town.
        // Record it.
        for (let k = 0; k < commonTowns.length; k++) {
          const town = commonTowns[k];
          if (_self.ants[antIndex].tour[0] === town || _self.ants[j].tour[0] === town) continue;
          const tourToTownAntI = _self.ants[antIndex].tour.slice(0, _self.ants[antIndex].tour.indexOf(town) + 1);
          const tourToTownAntJ = _self.ants[j].tour.slice(0, _self.ants[j].tour.indexOf(town) + 1);

          const timeAntI = _self.getTime(tourToTownAntI);
          const timeAntJ = _self.getTime(tourToTownAntJ);
          const timeDiff = timeAntI - timeAntJ;
          if (timeDiff <= _self.meetingTimeInterval && timeDiff >= -_self.meetingTimeInterval) {
            meetings.push({
              ants: [antIndex, j],
              town,
              time: timeAntI
            });
          }
        }
      }
    }
    if (index > -1) {
      if (this.ants[index].tour.length < 3) return meetings;
      getMeetingWithAnt(index);
    } else {
      for (let i = 0; i < this.ants.length; i++) {
        if (this.ants[i].tour.length < 3) continue;
        getMeetingWithAnt(i);
      }
    }


    return _uniqWith(meetings, (a, b) => { return a.ants[1] === b.ants[1] && a.ants[0] === b.ants[0]});
  }

  // Store in the probabilities object, the probability to move to each town.
  private probToTown(ant: Ant) {
    const currentTown = ant.tour[ant.tour.length - 1];
    let denominator = 0.0;

    // The accumulative denominator based on alg
    const keys = Object.keys(this.N[ant.travellerIndex]);

    this.probs[ant.travellerIndex] = {};
    for (let i = 0; i < keys.length; i++) {
      if (!ant.visited[keys[i]]) {
        denominator += Math.pow(this.trails[ant.travellerIndex][currentTown][keys[i]], this.alpha)
          * Math.pow(1.0 / this.graphs[ant.travellerIndex][currentTown][keys[i]], this.beta)
          * this.meetingProbability(ant, currentTown, keys[i]);
      }
    }

    // Set up transition probabilities
    for (let i = 0; i < keys.length; i++) {
      if (ant.visited[keys[i]]) {
        this.probs[ant.travellerIndex][keys[i]] = 0.0;
      } else {
        const numerator = Math.pow(this.trails[ant.travellerIndex][currentTown][keys[i]], this.alpha)
          * Math.pow(1.0 / this.graphs[ant.travellerIndex][currentTown][keys[i]], this.beta)
          * this.meetingProbability(ant, currentTown, keys[i]);
        this.probs[ant.travellerIndex][keys[i]] = numerator / denominator;
      }
    }

  }

  // Select next town based on the probabilities we assign to each town.
  // Also randomly chose one in the tablu list.
  private selectNextTown(ant: Ant) {
    if (Math.random() < this.pr) {
      const nextTown = Object.keys(this.N[ant.travellerIndex])[getRandomIntInclusive(0, Object.keys(this.N[ant.travellerIndex]).length - 1)];

      if (!ant.visited[nextTown]) {
        return nextTown;
      }
    }

    // calculate probs
    this.probToTown(ant);
    const r = Math.random();
    let tot = 0;

    // based on a random number pick the prob that matches.
    const objKeys = Object.keys(this.probs[ant.travellerIndex]);
    for (let i = 0; i < objKeys.length; i++) {
      tot += this.probs[ant.travellerIndex][objKeys[i]];
      if (tot >= r) {
        return objKeys[i];
      }
    }

    throw new Error('Not suppose to get here, error in "selectNextTown');
  }

  private updateTrails() {
    // evaporation pheromone on all trails
    for (let i = 0; i < this.ants.length; i++) {
      _forEach(this.trails[i], (trail, key) => {
        _forEach(trail, (_trailValue, _trailKey) => {
          _trailValue *= this.evaporation;
        });
      })
    }


    // update trails that have been traversed during a tour.
    // TODO increase trail pheromones if met at the same time with other travellers
    _forEach(this.ants, (ant) => {
      const contribution = this.Q / ant.getTourLength(this.graphs[ant.travellerIndex]);
      for (let i = 0; i < ant.tour.length - 1; i++) {
        this.trails[ant.travellerIndex][ant.tour[i]][ant.tour[i + 1]] += contribution;
      }

      this.trails[ant.travellerIndex][ant.tour[ant.tour.length - 1]][ant.tour[0]] += contribution;
    });
  }

  private initializeTrailsAndProbs() {
    for (let i = 0; i < this.ants.length; i++) {
      this.trails[i] = _merge({}, this.graph);
      if (!this.probs[i]) this.probs[i] = {};
      // may change this to make initial trails random
      _forEach(this.trails[i], (trail, key) => {
        _forEach(trail, (_trailValue, _trailKey) => {
          _trailValue = this.c;
        });
      })
    }
  }

  private getTourWithMeetings(meetings: any) {
    const tours = [];
    this.ants.forEach((ant) => {
      tours[ant.travellerIndex] = {
        tourLength: ant.getTourLength(this.graphs[ant.travellerIndex]),
        cost: (ant.tour && ant.tour.length > 1) ? this.getCost(ant.tour) : 0,
        tour: _clone(ant.tour),
        meetings: _filter(meetings, function(m) { return m.ants[0] === ant.travellerIndex; })
      };
    });

    return tours;
  }

  private getAverageTourLength() {
    let length = 0.0;
    this.ants.forEach((ant) => {
      length += ant.getTourLength(this.graphs[ant.travellerIndex]);
    });
    length = this.ants.length;
    return length;
}

  private updateBest() {
    this.ants.forEach((ant) => {
      const antTourLength = ant.getTourLength(this.graphs[ant.travellerIndex]);
      // initiate if best tour with meeting doesnt exist.
      if (!this.bestTours[ant.travellerIndex]) this.bestTours[ant.travellerIndex] = { tourLength: antTourLength + 999 };
      if (antTourLength < this.bestTours[ant.travellerIndex].tourLength) {
        this.bestTours[ant.travellerIndex] = {
          tourLength: ant.getTourLength(this.graphs[ant.travellerIndex]),
          cost: (ant.tour && ant.tour.length > 1) ? this.getCost(ant.tour) : 0,
          tour: _clone(ant.tour)
        };
      }
    });

    // save bestTourWitMeetings for all ants.
    // check if meetings happen
    // check if new avrg cost is better than old
    // save tours with meetings
    const meetings = this.getMeetings();
    if (meetings && meetings.length) {
      if (this.bestToursCostWithMeetings.cost === -1) {
        const bestTourInfo = {
          meetings,
          cost: this.getAverageTourLength()
        };
        const bestTour = this.getTourWithMeetings(meetings);

        // update best tour with meetings tracker
        this.bestToursCostWithMeetings = bestTourInfo;
        this.bestToursWithMeetings = bestTour;
        // update most meetings tracker
        this.bestToursCostWithMostMeetings = bestTourInfo;
        this.bestToursWithMostMeetings = bestTour;
      } else {
        const avrgcost = this.getAverageTourLength();
        // track tour with most meetings.
        if (meetings.length > this.bestToursCostWithMostMeetings.meetings.length || avrgcost < this.bestToursCostWithMeetings.cost) {
          const bestTour = this.getTourWithMeetings(meetings);
          if (meetings.length > this.bestToursCostWithMostMeetings.meetings.length) {
            this.bestToursCostWithMostMeetings.meetings = meetings;
            this.bestToursCostWithMostMeetings.cost = avrgcost;
            this.bestToursWithMostMeetings = bestTour;
          }

          if ( avrgcost < this.bestToursCostWithMeetings.cost) {
            this.bestToursCostWithMeetings.meetings = meetings;
            this.bestToursCostWithMeetings.cost = avrgcost;
            this.bestToursWithMeetings = bestTour;
          }
        }
      }
    }
  }

  /**
  * scaleGraphByPrefs - Uses the distance, time and cost along with
  *   the user prefs to determine the Graph edge lengths.
  */
  private scaleGraphByPrefs(graph: any, travellers: Array<any>) {
    // Assuming time to travel, cost to travel and distance are proportional.
    // Step 1: Get the max and min distance from the original graph
    // Step 2: getAllTowns, and then from townInfo get their Attractions and Countrylocated.
    // Step 3: Per traveller - create a map with userPrefToTown
    // Step 3.0: duplicate this.graph to be this.travellerGraphs[i] // i = traveller.index.
    // Step 3.1 if traveller has no preference make no changes continue to next
    // Step 3.2: compare traveller[i] to the found attractions and the countrylocation
    //          increment userPrefToTown if the town has it and if the town is located
    //          in the traveller's preferred country.
    // Step 3.3: calculate the base scale based on the Full Satisfaction to No satisfaction.
    // Step 3.4: Loop through the graphs and change the map distances by adding result from 4.2

    function scaledNum(num: number, minNum: number, maxNum: number) {
      return (num - minNum) / (maxNum - minNum);
    }

    function scaledBetweenTwoNums(num: number, minNum: number, maxNum: number) {
      return minNum + (num * (maxNum - minNum));
    }

    // updateDistances - update all distances going to the town with new distances based on Prefs.
    // eg: if townA -> townB on the graph and graphWithDistances[townB] =  99
    //     graph[townA][townB] += graphWithDistances[townB];
    //     for all incoming edges to townB.
    function updateDistances(originalGraph: any, graphWithDistances: any, prefWeight: number) {
      // console.log('Graph before: ', JSON.stringify(originalGraph));
      // console.log('update with graphWithDistances', JSON.stringify(graphWithDistances));
      for (const destKey in graphWithDistances) {
        for (const srcKey in originalGraph) {
          // skip if the srcKey is the key from the graphWithDistances
          if (destKey === srcKey) continue;
          originalGraph[srcKey][destKey] += (prefWeight * graphWithDistances[destKey]);
        }
      }

      // console.log('Graph after: ', JSON.stringify(originalGraph));
    }

    for (let i = 0; i < travellers.length; i++) {
      const travellerPrefs = 0;
      if (travellers[i]) {
        this.graphs[i] = _clone(graph);
        // continue to next traveller if this one has no preferences. (distances stay the same)
        if (!(travellers[i].activityPrefs && travellers[i].activityPrefs.length) &&
          !(travellers[i].countryPrefs && travellers[i].countryPrefs.length)) {
          continue;
        }

        // user has some preference.
        const maxPref = travellers[i].activityPrefs.length + travellers[i].countryPrefs.length;
        let activityPrefs, countryPrefs = [];
        const preferenceDistances = {};
        // set activity Prefs if they exists on the current traveller.
        if (travellers[i].activityPrefs && travellers[i].activityPrefs.length) activityPrefs = travellers[i].activityPrefs;

        // set country Prefs if they exists on the current traveller.
        if (travellers[i].countryPrefs && travellers[i].countryPrefs.length) countryPrefs = travellers[i].countryPrefs;

        // go through towns to see if their info match any preference
        for (const town in graph) {
          let townPref = 0;

          // if town in a country the user would like to go.
          if (this.townInfo[town] && this.townInfo[town].Country && _includes(countryPrefs, this.townInfo[town].Country)) {
            townPref++;
          }

          // if town has activities the user would like to do.
          const similarActivities = _intersectionWith(activityPrefs, this.townInfo[town].Activities, function (a, b) {
            return a.name === b;
          });

          if (similarActivities && similarActivities.length) {
            similarActivities.forEach((similarActivity) => {
              townPref += (similarActivity.weight / 10);
            });
          }

          const overallAttr = maxPref - townPref;
          preferenceDistances[town] = scaledBetweenTwoNums(scaledNum(overallAttr, 0, maxPref), this.graphMin, this.graphMax);
        }

        updateDistances(this.graphs[i], preferenceDistances, this.prefWeight);
      }
    }
  }

  public solve() {
    const solutions = [];
    const startTime = new Date();
    console.log('---------------------------');
    console.log(`Solving [${this.travellers.length}] travellers: through [${Object.keys(this.graph)}] ${Object.keys(this.graph).length} cities`);
    console.log(`${Object.keys(this.graph).length} cities`);
    console.log(`Total Iterations: ${this.maxIterations}`);
    console.log('---------------------------');
    this.initializeTrailsAndProbs();
    this.scaleGraphByPrefs(this.graph, this.travellers);
    let iteration = 0;

    while (iteration < this.maxIterations) {
      this.setupAnts();
      this.moveAnts();
      this.updateTrails();
      this.updateBest();
      iteration++;

      if (iteration % 200 === 0) {
        this.ants.forEach((ant) => {
          console.log('---');
          console.log(`Best tourLength for traveller ${ant.travellerIndex} @ iteration : ${iteration}`, this.bestTours[ant.travellerIndex].tourLength);
          console.log(`Best tour for traveller ${ant.travellerIndex} @ iteration : ${iteration}`, JSON.stringify(this.bestTours[ant.travellerIndex].tour));
        })
      }
    }

    const endTime = new Date();
    console.log('---------------------------');
    console.log('Solution: ', JSON.stringify(this.bestTours));
    console.log('---------------------------');
    console.log('Solution with Meetings: ', JSON.stringify(this.bestToursWithMeetings));
    console.log('---------------------------');
    console.log('Solution with MOST Meetings: ', JSON.stringify(this.bestToursWithMostMeetings));
    console.log('---------------------------');
    console.log(`End Time: ${endTime}`);
    console.log(`Time lapse: ${ endTime.getTime() - startTime.getTime()}ms`);
    console.log('---------------------------');


    return _clone({ bestTours: this.bestTours, bestToursWithMeetings: this.bestToursWithMeetings, bestToursWithMostMeetings: this.bestToursWithMostMeetings });
  }

}

/**
 * getRandomIntInclusive
 *  - Utility function for random values
 *    between two numbers
 *
 * @param {number} min
 * @param {number} max
 * @returns
 */
function getRandomIntInclusive(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; // The maximum is inclusive and the minimum is inclusive
}


export { ModifiedAco };
