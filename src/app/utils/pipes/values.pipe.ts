import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'values'})
export class ValuesPipe implements PipeTransform {
    transform(value: any, args?: any[]): Object[] {
        const keyArr: any[] = Object.keys(value),
            dataArr = [];

        keyArr.forEach((key: any) => {
            value[key]['__key'] = key;
            dataArr.push(value[key])
        });

        return dataArr;
    }
}
