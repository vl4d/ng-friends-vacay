import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AccordionModule, ButtonsModule } from 'ngx-bootstrap';
import { LoadingModule } from 'ngx-loading';
import { BarRatingModule } from 'ngx-bar-rating';
import { FormWizardModule } from './configure/wizard';
import { AppRoutingModule } from './app-routing.module';
import { AmChartsModule } from '@amcharts/amcharts3-angular';
import { SelectModule } from 'ng-select';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ConfigureComponent } from './configure/configure.component';

import { ConfigWizardComponent } from './configure/config-wizard.component';
import { UserListComponent } from './configure/user-list/user-list.component';
import { UserPrefsComponent } from './configure/user-prefs/user-prefs.component';
import { CountrySelectorComponent } from './configure/user-prefs/country-selector.component';

import { ValuesPipe } from './utils/pipes/values.pipe';
import { AcoResultsComponent } from './configure/aco-results/aco-results.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    FormWizardModule,
    AccordionModule.forRoot(),
    ButtonsModule.forRoot(),
    AppRoutingModule,
    BarRatingModule,
    LoadingModule,
    SelectModule,
    AmChartsModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    ConfigureComponent,
    ConfigWizardComponent,
    NavComponent,
    FooterComponent,
    UserListComponent,
    UserPrefsComponent,
    CountrySelectorComponent,
    ValuesPipe,
    AcoResultsComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
