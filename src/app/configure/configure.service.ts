import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import _findIndex from 'lodash/findIndex';
import _clone from 'lodash/clone';

import { ModifiedAco } from './../scripts/modified-aco';
import * as towns from './../../data/24cities/towns24.json';
import * as townInfo from './../../data/24cities/townsInfo24.json';
import * as townActivities from './../../data/townActivities.json';

@Injectable()
export class ConfigureService {
  activities: Array<any>;
  users: Array<any>;

  static getCountries() {
    return [{'id': 'AL'}, {'id': 'AD'}, {'id': 'AT'}, {'id': 'BY'}, {'id': 'BE'}, {'id': 'BA'}, {'id': 'BG'}, {'id': 'HR'}, {'id': 'CY'}, {'id': 'CZ'}, {'id': 'DK'}, {'id': 'EE'}, {'id': 'FO'}, {'id': 'FI'}, {'id': 'FR'}, {'id': 'DE'}, {'id': 'GI'}, {'id': 'GR'}, {'id': 'HU'}, {'id': 'IE'}, {'id': 'IM'}, {'id': 'IT'}, {'id': 'LV'}, {'id': 'LI'}, {'id': 'LT'}, {'id': 'LU'}, {'id': 'MK'}, {'id': 'MT'}, {'id': 'MD'}, {'id': 'MC'}, {'id': 'ME'}, {'id': 'NL'}, {'id': 'NO'}, {'id': 'PL'}, {'id': 'PT'}, {'id': 'RO'}, {'id': 'RS'}, {'id': 'SM'}, {'id': 'SK'}, {'id': 'SI'}, {'id': 'ES'}, {'id': 'SE'}, {'id': 'CH'}, {'id': 'UA'}, {'id': 'GB'}, {'id': 'VA'}, {'id': 'TR'}];
  }

  static getACOGraph() {
    return towns;
  }

  static getACOTownInfo() {
    return townInfo;
  }

  static convertUsersToTravellers(users: any) {
    const travellers = [];
    users.forEach((user) => {
      const temp = _clone(user);
      temp.time = user.days * 24 * 60;
      temp.activityPrefs = [];
      for (const pref in user.activityPrefs) {
        if (!user.activityPrefs[pref].selected) continue;
        temp.activityPrefs.push(pref);
      }
      travellers.push(temp);
    });
    return travellers;
  }

  constructor() {
    this.users = [
      {
        _id: '1',
        name: 'Vlad-Tester',
        activityPrefs: [],
        countryPrefs: [],
        budget: 600.00,
        days: 7,
        startTown: 'Warsaw'
      },

      {
        _id: '2',
        name: 'User1-Tester',
        activityPrefs: ['historical buildings'],
        countryPrefs: [],
        budget: 600.00,
        days: 7,
        startTown: 'London'
      },
      // {
      //   _id: '3',
      //   name: 'User2-Tester',
      //   activityPrefs: ['museum', 'zoo'],
      //   countryPrefs: [],
      //   budget: 700.00,
      //   days: 7,
      //   startTown: 'Budapest'
      // }
    ];

    this.activities = [].concat(townActivities);
  }

  generateACOResults() {
    const travellers = ConfigureService.convertUsersToTravellers(this.users);
    const modifiedACO = new ModifiedAco(ConfigureService.getACOGraph(), ConfigureService.getACOTownInfo(), travellers);
    return modifiedACO.solve();
  }

  getActivities() {
    return this.activities;
  }

  getUsers() {
    return this.users;
  }

  addUser(user) {
    if (user && _findIndex(this.users, {_id: user._id}) === -1) {
      user.budget = 600.00;
      user.days = 7;
      if (!user._id) user._id = UUID.UUID();
      this.users.push(user);
      return user;
    }

    return user;
  }

  removeUser(user) {
    if (user) {
      const index = _findIndex(this.users, {_id: user._id});
      // remove item using index found
      this.users.splice(index, 1);
    }

    return this.users;
  }

  updateUsers(users) {
    this.users = users;
  }

  updateUserActivityPrefs(id, activityPrefs) {
    const index = _findIndex(this.users, {_id: id});
    // update item using index found
    if (index > -1) this.users[index].activityPrefs = activityPrefs;
  }

  updateUserCountryPrefs(id, countryPrefs) {
    const index = _findIndex(this.users, {_id: id});
    // update item using index found
    if (index > -1) this.users[index].countryPrefs = countryPrefs;
  }
}
