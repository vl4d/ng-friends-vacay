import { Component, Input, OnInit, OnDestroy, AfterViewInit, Output, EventEmitter  } from '@angular/core';
import { AmChartsService } from '@amcharts/amcharts3-angular';
import { ConfigureService } from 'app/configure/configure.service';
import { ANIMATION_TYPES } from 'ngx-loading';

@Component({
  selector: 'app-aco-results',
  templateUrl: './aco-results.component.html',
  styleUrls: ['./aco-results.component.scss']
})
export class AcoResultsComponent implements OnInit, AfterViewInit, OnDestroy {

  public ANIMATION_TYPES = ANIMATION_TYPES;
  public travellerIteneraries: Array<any> = [];
  private chart: any;
  private resultsFromACO: any;
  private users: Array<any> = [];
  private showitInerary: Array<boolean> = [];
  private townInfo: any;
  /**
   * SVG path for target icon
   */
  // tslint:disable-next-line:max-line-length
  private targetSVG = 'M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z';

  /**
   * SVG path for plane icon
   */
  private planeSVG = 'm2,106h28l24,30h72l-44,-133h35l80,132h98c21,0 21,34 0,34l-98,0 -80,134h-35l43,-133h-71l-24,30h-28l15,-47';

  public loading = true;

  @Output() onToggleCompleted: EventEmitter<any> = new EventEmitter<any>();

  constructor(private AmCharts: AmChartsService, private _configService: ConfigureService) {
    this.users = _configService.getUsers();
    for (let i = 0; i < this.users.length; i++) {
      this.showitInerary[i] = false;
    }

    this.townInfo = ConfigureService.getACOTownInfo();
   }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.setUpMap();
    }, 200);
  }

  toggleCompleted() {
    this.onToggleCompleted.emit(false);
  }

  /**
   * [
   *  {
   *    "tourLength":5958.76,
   *    "cost":650,"tour": ["Warsaw","Berlin","Hamburg","Munich","Prague","Warsaw"]
   *  },
   *  {
   *    "tourLength":0,
   *    "cost":0,"tour": ["Istanbul"]
   *  }
   * ]
   */
  getMapLinesAndImages() {
    // {
    //   "id": "line1",
    //   "arc": -0.5,
    //   "alpha": 0.3,
    //   "latitudes": [ 52.2297, 47.4979, 48.2082, 50.0755, 52.2297 ],
    //   "longitudes": [ 21.0122, 19.0402, 16.3738, 14.4378, 21.0122 ]
    // }
    const users = this.users;
    function getRandomColor() {
      const letters = '0123456789ABCDEF';
      let color = '#';
      for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    }

    function getMeetups(i: number, meetings: Array<any>) {
        const meetups = {};
        if (meetings && meetings.length) {
          meetings.forEach((m) => {
            if (m.ants[0] === i) {
              if (!meetups[m.town]) meetups[m.town] = [];
              meetups[m.town].push(users[m.ants[1]].name);
            }
          });
        }

        return meetups;
    }

    const lines = [];
    const images = [];
    if (this.resultsFromACO && this.resultsFromACO.bestTours && this.resultsFromACO.bestTours.length) {
      let tour = this.resultsFromACO.bestToursWithMostMeetings;
      if (!tour.length) tour = this.resultsFromACO.bestTours;
      let line = 0;
      const townsHash = {};
      tour.forEach((acoTour) => {
        const lats = [];
        const longs = [];
        const randomColor = getRandomColor();

        this.travellerIteneraries[line] = {
          acoTour,
          tourColour: randomColor,
          meetups: getMeetups(line, acoTour.meetings)
        };
        acoTour.tour.forEach((town) => {
          if (!townsHash[town]) townsHash[town] = true;
          lats.push(this.townInfo[town].latlng[0]);
          longs.push(this.townInfo[town].latlng[1])
        });
        const lineObj = {
          'id': `line${line}`,
          'arc': -0.5,
          'alpha': 0.8,
          'color': randomColor,
          'thickness': 2,
          'latitudes': lats,
          'longitudes': longs
        };

        lines.push(lineObj);
        images.push({
          'svgPath': this.planeSVG,
          'positionOnLine': 0,
          'color': randomColor,
          'animateAlongLine': true,
          'lineId': lineObj.id,
          'loop': false,
          'scale': 0.01,
          'positionScale': 1.8
        });

        line++;
      });
      for (const key in townsHash) {
        if (townsHash[key]) {
          images.push({
            'svgPath': this.targetSVG,
            'title': key,
            'alpha': 0.8,
            'scale': 0.8,
            'latitude': this.townInfo[key].latlng[0],
            'longitude': this.townInfo[key].latlng[1]
          });
        }
      }
    }


    return {lines, images};
  }

  setUpMap() {
    // TODO
    // 0 - store town info and graph in configure service? (done)
    // 1 - get the townInfo (done)
    // 2 - Format the user preferences. (done)
    // 3 - Get the graph (done)
    // 4 - Run the ModifiedACO with the required info // loading circle while waiting to compute (done)
    // 5 - take results and per user:
    // 5.1 - get the town lat/long coords and their name. (done)
    // 5.2 - draw a line and fill in the array per each. (done)
    // 5.4 - make the line a diff colour. (done)
    // 6 - TODO Show the cost, and activities that would be done if visiting those countries.
    // 7 - Highlight spots where the meet will happen with a red mark.
    // 8 - A key to assign each person a colour.
    const europeanCountries = ConfigureService.getCountries();
    this.resultsFromACO = this._configService.generateACOResults();
    console.log(this.resultsFromACO);
    // get line path per tour & images on the line path and town targest
    const linesAndImages = this.getMapLinesAndImages();

    const mapSettings = {
        type: 'map',
        theme: 'light',
        projection: 'winkel3',
        dataProvider: {
          map: 'worldHigh',
          'zoomLevel': 5,
          'zoomLongitude': 10,
          'zoomLatitude': 52,
          'areas': europeanCountries,
          'lines': linesAndImages.lines,
          'images': linesAndImages.images
        },

        areasSettings: {
            'rollOverOutlineColor': '#FFFFFF',
            'rollOverColor': '#027262',
            'alpha': 0.8,
            'unlistedAreasAlpha': 0
        },

        'imagesSettings': {
          'color': '#fffff',
          'rollOverColor': '#fffff',
          'selectedColor': '#fffff',
          'pauseDuration': 0.2,
          'animationDuration': 8,
          'adjustAnimationSpeed': true
        },

        'linesSettings': {
          'color': '#585869',
          'alpha': 0.4
        },

      'export': {
        'enabled': true
      }
    };

    // Draw map;
    this.chart = this.AmCharts.makeChart('results-chart', mapSettings);

    // Stop loading screen
    this.loading = false;
  }

  ngOnDestroy() {
        this.AmCharts.destroyChart(this.chart);
    }

}

