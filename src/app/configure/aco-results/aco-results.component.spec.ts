import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcoResultsComponent } from './aco-results.component';

describe('AcoResultsComponent', () => {
  let component: AcoResultsComponent;
  let fixture: ComponentFixture<AcoResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcoResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcoResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
