import { Component, OnInit } from '@angular/core';
import { ConfigureService } from 'app/configure/configure.service';

@Component({
  selector: 'app-configure',
  templateUrl: './configure.component.html',
  providers: [ConfigureService]
})

export class ConfigureComponent implements OnInit {
  stepsList: Array<any> = [];
  users: Array<any> = [];
  configuratedTour: Array<any> = [];
  stepAdded: String;
  configService: ConfigureService;
  isCompleted = false;

  constructor(private _configService: ConfigureService) {
    this.users = _configService.getUsers();
    // Setup existing users from service
    this.setUpUserSteps();
  }

  ngOnInit() {
  }

  setUpUserSteps() {
    this.stepsList.splice(0, this.stepsList.length);
    this.users.forEach((user) => {
      this.addNewUserStep(user);
    });
  }

  triggerStepsChanged(stepAdded) {
    setTimeout(() => {
      console.log('triggerStepsChanged: ', stepAdded);
      this.stepAdded = stepAdded;
    }, 500);
  }

  toggleCompleted() {
    this.isCompleted = false;
  }

  onStepAddedToWizard(event) {
    this.addNewUserStep(event);
  }

  onStepRemovedFromWizard(event) {
    this.removeUserStep(event);
  }

  onWizardComplete($event) {
    this.isCompleted = true;
  }

  removeUserStep(userStep) {
    console.log('removeUserStep');
    if (userStep && userStep.user) {
      this.stepsList.splice(0, this.stepsList.length);
      const newUserArr = this._configService.removeUser(userStep.user);

      if (newUserArr.length) {
        newUserArr.forEach((user) => {
          this.addNewUserStep(user);
        });
      } else {
        // refresh list.
        this.triggerStepsChanged('all-removed');
      }
    }
  }

  addNewUserStep(user) {
    // console.log('addNewUserStep');
    // Add user to service list and to steps
    const usr = {
      title: user.name || 'default',
      user: this._configService.addUser(user) || {},
      showNext: true,
      showPrev: true
    };
    this.stepsList.push(usr);
    this.triggerStepsChanged(user.name);
  }

}
