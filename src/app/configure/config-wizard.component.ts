import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import _filter from 'lodash/filter';

@Component({
  selector: 'app-config-wizard',
  templateUrl: './config-wizard.component.html'
})
export class ConfigWizardComponent implements OnInit {
  @Input() dynamicSteps: Array<any> = [];
  @Input() changedStep: String;
  @Input() isCompleted = false;
  @Output() onNext: EventEmitter<any> = new EventEmitter<any>();
  @Output() onPrev: EventEmitter<any> = new EventEmitter<any>();
  @Output() onStepAddedToWizard: EventEmitter<any> = new EventEmitter<any>();
  @Output() onStepRemovedFromWizard: EventEmitter<any> = new EventEmitter<any>();
  @Output() onWizardComplete: EventEmitter<any> = new EventEmitter<any>();

  newUserName = '';
  firstStep: any = {
    title: 'Friend\'s List',
    showNext: true,
    showPrev: false
  };
  lastStep: any = {
    title: 'Preference Summary',
    showNext: false,
    showPrev: true
  };

  constructor() { }

  ngOnInit() {
  }

  addUser() {
    const newUser = {
      name: this.newUserName,
      activityPrefs: [],
      countryPrefs: []
    };

    this.onStepAddedToWizard.emit(newUser);
    this.newUserName = '';

  }

  getCheckedActivities(activities) {
    return _filter(activities, {selected: true});
  }

  onfirstStepNext(event) {
    console.log('firstStep - Next');
  }

  onComplete(event) {
    this.isCompleted = true;
    this.onWizardComplete.emit(this.isCompleted);
  }

  onStepChanged(step) {
    console.log('Changed to ' + step.title);
  }

  onStepRemoved(user) {
    this.onStepRemovedFromWizard.emit(user);
  }

}
