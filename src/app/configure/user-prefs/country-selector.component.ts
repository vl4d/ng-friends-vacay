import { Component, Input, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { AmChartsService } from '@amcharts/amcharts3-angular';

import { ConfigureService } from 'app/configure/configure.service';

@Component({
    selector: 'app-country-selector',
    template: `<div class="row" id="chart{{user._id}}" [style.width.%]="100" [style.height.px]="500"></div>`
})
export class CountrySelectorComponent implements OnInit, AfterViewInit, OnDestroy {
    private chart: any;
    selectedCountries: Array<any> = [];
    @Input() user: any;

    constructor(private AmCharts: AmChartsService, private _configService: ConfigureService) { }

    getSelectedCountries() {
        const selected = [];
        for (let i = 0; i < this.chart.dataProvider.areas.length; i++) {
            if (this.chart.dataProvider.areas[i].showAsSelected)
                selected.push(this.chart.dataProvider.areas[i].title);
        }
        return selected;
    }

    ngOnInit() { }

    ngAfterViewInit() {
        const europeanCountries = ConfigureService.getCountries();
        this.chart = this.AmCharts.makeChart('chart' + this.user._id, {
            type: 'map',
            theme: 'light',
            dataProvider: {
                map: 'worldHigh',
                'zoomLevel': 3.5,
                'zoomLongitude': 10,
                'zoomLatitude': 52,
                'areas': europeanCountries
            },
            areasSettings: {
                selectedColor: '#CC0000',
                selectable: true,
                'rollOverOutlineColor': '#FFFFFF',
                'rollOverColor': '#CC0000',
                'alpha': 0.8,
                'unlistedAreasAlpha': 0
            },
            // Add click event to track country selection/unselection
            listeners: [{
                event: 'clickMapObject',
                method: (e) => {
                    // Ignore any click not on area
                    if (e.mapObject.objectType !== 'MapArea')
                        return;

                    const area = e.mapObject;

                    // Toggle showAsSelected
                    area.showAsSelected = !area.showAsSelected;
                    e.chart.returnInitialColor(area);

                    // Update the list
                    this.selectedCountries = this.getSelectedCountries();
                    this.user.countryPrefs = this.selectedCountries;
                }
            }],
            export: {
                enabled: true
            }
        });
    }

    ngOnDestroy() {
        this.AmCharts.destroyChart(this.chart);
    }
}
