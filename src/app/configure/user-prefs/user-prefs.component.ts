import { Component, Input, OnInit, AfterViewInit } from '@angular/core';
import { IOption } from 'ng-select';
import { ConfigureService } from 'app/configure/configure.service';
import _filter from 'lodash/filter';

@Component({
  selector: 'app-user-prefs',
  templateUrl: './user-prefs.component.html',
  styleUrls: ['./user-prefs.component.scss']
})
export class UserPrefsComponent implements OnInit, AfterViewInit {
  oneAtATime = true;
  checkedActivities: any = {};
  activities: Array<any> = [];
  townOptions: Array<IOption> = [];

  @Input() user: any;

  constructor(private _configService: ConfigureService) {
    this.activities = _configService.getActivities();
    this.activities.forEach((activity) => {
      this.checkedActivities[activity.name] = {
        name: activity.name,
        selected: false,
        weight: 1
      };
    });

  }

  ngAfterViewInit() {
    this.user.activityPrefs = this.checkedActivities;
  }

  ngOnInit() {
    const townInfo = ConfigureService.getACOTownInfo();
    for (const town in townInfo) {
      this.townOptions.push({
        label: `${town}, ${townInfo[town].Country}`,
        value: town
      });
    }
  }

  getTruePrefs() {
    return _filter(this.checkedActivities, {selected: true});
  }

}
