# NgFriendsVacay

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.1.

Created by Vladimir Murray (vl4d) for my MSc. Individual Project @ King's College London.

An application of the Ant Colony Optimisation Method with Linear Programming to 
optimise scheduling and planning with intemidiatary meetings amongst ants, at the same node at the same time.

Access is available here: [https://vl4d.gitlab.io/ng-friends-vacay/](https://vl4d.gitlab.io/ng-friends-vacay/)

European Friends Vacation planner.

### Other libraries used
- AngularJs 4 (UI Framework)
- AmCharts (For Maps)
- Lodash (Utility library for dealing with javascript objects)
- Node (Javascript framework)
- Font-Awesome (Icons)
- Ngx-Bar-Rating (UI component for rating)
- Ngx-Bootstrap (AngularJs 4 implementation of Bootstrap 3, used for UI styling)
- Ngx-loadingng (Loading Screen)

### TODO
- [x] The ACO with Meetings library
- [x] Global State Management (needed?)
- [x] Saving data when finished a step to the service store.
- [x] Results Page to show route summary
- [x] Dummy Data generator to quickly run without going through setup process. 
- [x] Load Testing


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
